#!/bin/sh

#set -x # debug

PHPFPM_PIDFILE=/run/php-fpm7.pid
LIGHTTPD_PIDFILE=/run/lighttpd.pid

start() {
	STARTER="${1}"
	shift
	chown nobody:nobody /home /home/roundcubemail.db 2>/dev/null || true # already chowned during build (roundcube DB storage), however, it must be rechowned in the case of a volume mounted there
	php-fpm7 --pid "${PHPFPM_PIDFILE}" --nodaemonize --force-stderr --allow-to-run-as-root & echo ${!} >> "${PHPFPM_PIDFILE}" # must be started in foreground to log into stdout/stderr
	${STARTER} lighttpd -f /etc/lighttpd/lighttpd.conf $@
}

stop() {
	LIGHTTPD_PID=$(head -n 1 "${LIGHTTPD_PIDFILE}")
	PHPFPM_PID=$(head -n 1 "${PHPFPM_PIDFILE}")
	kill -SIGTERM ${LIGHTTPD_PID} ${PHPFPM_PID}
	wait ${LIGHTTPD_PID} ${PHPFPM_PID} || true # ignore if wait exits by 127 because the PIDs are already killed
}

term_handler() {
	stop
	exit 143 # 128 + 15 -- SIGTERM
}

case "${1}" in
start )
	start
	;;
start-foreground )
	start
	trap 'kill ${!}; term_handler' SIGTERM
	while true; do tail -f /dev/null & wait ${!}; done
	;;
start-foreground-notrap )
	start exec -D
	;;
stop )
	stop
	;;
--help )
	echo "Usage: ${0} <start|start-foreground|start-foreground-notrap|stop|--help>" >&2
	exit 1
	;;
* )
	start
	trap 'term_handler' SIGTERM
	$@
	stop
esac
