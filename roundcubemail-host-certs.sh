#!/bin/sh

LIST=${0%.sh}.txt
OUTPUT=home/$(basename ${0} .sh).pem
CERT_BEGIN="-----BEGIN CERTIFICATE-----"
CERT_END="-----END CERTIFICATE-----"

echo -n > "${OUTPUT}"

cat "${LIST}" | while read NAME; do
	case "${NAME}" in
	http* )
		wget "${NAME}" -O - >> "${OUTPUT}"
		;;
	*)
		echo "${CERT_BEGIN}" >> "${OUTPUT}"
		echo \
		| openssl s_client -showcerts -servername "${NAME}" -connect "${NAME}" \
		| sed -e "1,/${CERT_BEGIN}/d" -e "/${CERT_END}/,\$d" >> "${OUTPUT}"
		echo "${CERT_END}" >> "${OUTPUT}"
	esac
done

#exec openssl x509 -in "${OUTPUT}" -text
